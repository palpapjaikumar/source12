﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using InsproDataAccess;
using System.Text;
using System.Drawing;


public partial class StudentGpaCgpaDetails : System.Web.UI.Page
{
    InsproDirectAccess dir = new InsproDirectAccess();
    InsproStoreAccess storAcc = new InsproStoreAccess();
    DataTable dtHeader = new DataTable();
    string collegecode = string.Empty;
    string singleuser = string.Empty;
    string group_user = string.Empty;
    string usercode = string.Empty;
    string group_code = string.Empty;
    DataSet ds = new DataSet();
    DAccess2 da = new DAccess2();
    Dictionary<string, string> dicParameter = new Dictionary<string, string>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["collegecode"] == null)
        {
            Response.Redirect("~/Default.aspx");
        }
        //if (!Request.FilePath.Contains("CAMHome"))
        //{
        //    string strPreviousPage = "";
        //    if (Request.UrlReferrer != null)
        //    {
        //        strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
        //    }
        //    if (strPreviousPage == "")
        //    {
        //        Response.Redirect("~/MarkMod/CAMHome.aspx");
        //        return;
        //    }
        //}
        if (!IsPostBack)
        {
            GridView1.Visible = false;
            bindclg();
            BindBatch();
            bindEduLevel();
            BindDegree();
            bindbranch();
        }
    }
    public void bindclg()
    {
        try
        {
            string columnfield = string.Empty;
            group_code = Session["group_code"].ToString();
            if (group_code.Contains(';'))
            {
                string[] group_semi = group_code.Split(';');
                group_code = group_semi[0].ToString();
            }
            if ((group_code.ToString().Trim() != "") && (Session["single_user"].ToString() != "1" && Session["single_user"].ToString() != "true" && Session["single_user"].ToString() != "TRUE" && Session["single_user"].ToString() != "True"))
            {
                columnfield = " and group_code='" + group_code + "'";
            }
            else
            {
                columnfield = " and user_code='" + Session["usercode"] + "'";
            }
            dicParameter.Clear();
            dicParameter.Add("column_field", columnfield.ToString());
            //dtHeader.Dispose();
            //dtHeader.Clear();
            dtHeader = storAcc.selectDataTable("bind_college", dicParameter);

            ddlCollege.Items.Clear();

            if (dtHeader.Rows.Count > 0)
            {
                ddlCollege.DataSource = dtHeader;
                ddlCollege.DataTextField = "collname";
                ddlCollege.DataValueField = "college_code";
                ddlCollege.DataBind();
            }

        }
        catch
        {
        }
    }

    public void BindBatch()
    {
        dicParameter.Clear();
        ddlbatch.Items.Clear();
        //dtHeader.Dispose();
        //dtHeader.Clear();
        dtHeader = storAcc.selectDataTable("bind_batch", dicParameter);
        int count = dtHeader.Rows.Count;
        if (count > 0)
        {
            ddlbatch.DataSource = dtHeader;
            ddlbatch.DataTextField = "batch_year";
            ddlbatch.DataValueField = "batch_year";
            ddlbatch.DataBind();
        }
        int count1 = dtHeader.Rows.Count;
        if (count > 0)
        {
            int max_bat = 0;
            max_bat = Convert.ToInt32(dtHeader.Rows[0][0].ToString());
            ddlbatch.SelectedValue = max_bat.ToString();
        }
    }

    public void BindDegree()
    {

        ddldegree.Items.Clear();
        collegecode = Convert.ToString(ddlCollege.SelectedValue);
        singleuser = Session["single_user"].ToString();
        group_user = Session["group_code"].ToString();
        //dtHeader.Dispose();
        //dtHeader.Clear();
        if (!string.IsNullOrEmpty(Convert.ToString(ddlEdulevel.SelectedValue)))
        {
            string sql_s = "select distinct course_name,course_id from course where college_code='" + collegecode + "'  and Edu_Level='" + Convert.ToString(ddlEdulevel.SelectedValue) + "'";
            dtHeader = dir.selectDataTable(sql_s);
            if (dtHeader.Rows.Count > 0)
            {
                ddldegree.DataSource = dtHeader;
                ddldegree.DataTextField = "course_name";
                ddldegree.DataValueField = "course_id";
                ddldegree.DataBind();
            }
        }
    }

    public void bindbranch()
    {
        dicParameter.Clear();
        ddlbranch.Items.Clear();
        usercode = Session["usercode"].ToString();
        collegecode = Convert.ToString(ddlCollege.SelectedValue); ;
        singleuser = Session["single_user"].ToString();
        group_user = Session["group_code"].ToString();
        if (group_user.Contains(';'))
        {
            string[] group_semi = group_user.Split(';');
            group_user = group_semi[0].ToString();
        }
        //dicParameter.Add("single_user", singleuser);
        //dicParameter.Add("group_code", group_user);
        //dicParameter.Add("course_id", ddldegree.SelectedValue);
        //dicParameter.Add("college_code", collegecode);
        //dicParameter.Add("user_code", usercode);
        ds.Dispose();
        ds.Reset();
        ds = da.BindBranchMultiple(singleuser, group_user, ddldegree.SelectedValue.ToString(), collegecode, usercode);
        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            ddlbranch.DataSource = ds;
            ddlbranch.DataTextField = "dept_name";
            ddlbranch.DataValueField = "degree_code";
            ddlbranch.DataBind();
        }

    }

    public void bindEduLevel()
    {
        try
        {
            collegecode = Convert.ToString(ddlCollege.SelectedValue);
            ddlEdulevel.ClearSelection();
            ddlEdulevel.Items.Clear();
            string sql_s = "select distinct Edu_Level from course where college_code='" + collegecode + "'";
            dtHeader = dir.selectDataTable(sql_s);
            if (dtHeader.Rows.Count > 0)
            {
                ddlEdulevel.DataSource = dtHeader;
                ddlEdulevel.DataTextField = "Edu_Level";
                ddlEdulevel.DataValueField = "Edu_Level";
                ddlEdulevel.DataBind();
            }
        }
        catch
        {
        }
    }


    protected void ddlCollege_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindBatch();
        bindEduLevel();
        BindDegree();
        bindbranch();
        GridView1.Visible = false;


    }

    protected void ddlbatch_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridView1.Visible = false;
    }

    protected void ddldegree_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindbranch();
        GridView1.Visible = false;
    }

    protected void ddlbranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridView1.Visible = false;
    }

    protected void ddlEdulevel_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BindDegree();
            bindbranch();
            GridView1.Visible = false;
        }
        catch
        { }
    }

    protected void GO_Click(object sender, EventArgs e)
    {
        try
        {
            GridView1.Visible = false;
            string eduLevel = Convert.ToString(ddlEdulevel.SelectedValue);
            string batchYear = Convert.ToString(ddlbatch.SelectedValue);
            string degreeCode = Convert.ToString(ddlbranch.SelectedValue);
            string collCode = Convert.ToString(ddlCollege.SelectedValue);
            DataTable dtReport = new DataTable();
            StringBuilder PartName;
            DataRow drNew = null;
            dtReport.Columns.Add("S.No");
            dtReport.Columns.Add("Roll No");
            dtReport.Columns.Add("Register No");
            dtReport.Columns.Add("Student Name");

            drNew = dtReport.NewRow();
            drNew["S.No"] = "S.No";
            drNew["Roll No"] = "Roll No";
            drNew["Register No"] = "Register No";
            drNew["Student Name"] = "Student Name";
            dtReport.Rows.Add(drNew);
            drNew = dtReport.NewRow();
            drNew["S.No"] = "S.No";
            drNew["Roll No"] = "Roll No";
            drNew["Register No"] = "Register No";
            drNew["Student Name"] = "Student Name";
            dtReport.Rows.Add(drNew);

            string Part = "select distinct Part_Type from subject s,syllabus_master sy where s.syll_code=sy.syll_code and sy.batch_year='" + batchYear + "' and sy.degree_code='" + degreeCode + "' and Part_Type is not null order by Part_Type";
            DataTable dtSubPart = dir.selectDataTable(Part);
            if (dtSubPart.Rows.Count > 0)
            {
                foreach (DataRow drCo in dtSubPart.Rows)
                {

                    //PartName = new System.Text.StringBuilder(criteria);
                    string PartNo = Convert.ToString(drCo["Part_Type"]);
                    //dtReport.Columns.Add("Part" + PartNo);
                    string val = "Part" + "-" + PartNo;
                    PartName = new System.Text.StringBuilder(val);
                    AddTableColumn(dtReport, PartName);
                    dtReport.Rows[dtReport.Rows.Count - 2][dtReport.Columns.Count - 1] = PartName;
                    dtReport.Rows[dtReport.Rows.Count - 1][dtReport.Columns.Count - 1] = "CGPA";
                    AddTableColumn(dtReport, PartName);
                    dtReport.Rows[dtReport.Rows.Count - 2][dtReport.Columns.Count - 1] = PartName;
                    dtReport.Rows[dtReport.Rows.Count - 1][dtReport.Columns.Count - 1] = "GRADE";
                    AddTableColumn(dtReport, PartName);
                    dtReport.Rows[dtReport.Rows.Count - 2][dtReport.Columns.Count - 1] = PartName;
                    dtReport.Rows[dtReport.Rows.Count - 1][dtReport.Columns.Count - 1] = "GP";
                }
            }
            string selectQ = "select r.Stud_Name, m.roll_no,r.Reg_No,r.App_No,s.Part_Type,SUM(s.credit_points) as EarnedCredit,SUM(ROUND(case when ISNULL(s.maxtotal,'0')<> '0' then (case when isnull(m.total,0) >=0  then isnull(m.total,0) else 0 end * 10)/s.maxtotal else 0 end,1,0)* s.credit_points) as GP,case when ISNULL(SUM(s.credit_points),'0')<>'0' then SUM(ROUND(case when ISNULL(s.maxtotal,'0')<> '0' then (case when isnull(m.total,0) >=0  then isnull(m.total,0) else 0 end * 10)/s.maxtotal else 0 end,1,0)* s.credit_points)/SUM(s.credit_points) else '0' end as GPA,ROUND(case when ISNULL(SUM(s.credit_points),'0')<>'0' then SUM(ROUND(case when ISNULL(s.maxtotal,'0')<> '0' then (case when isnull(m.total,0) >=0  then isnull(m.total,0) else 0 end * 10)/s.maxtotal else 0 end,1,0)* s.credit_points)/SUM(s.credit_points) else '0' end,3,0) as GPAResult,ROUND(case when ISNULL(SUM(s.credit_points),'0')<>'0' then SUM(ROUND(case when ISNULL(s.maxtotal,'0')<> '0' then (case when isnull(m.total,0) >=0  then isnull(m.total,0) else 0 end * 10)/s.maxtotal else 0 end,1,0)* s.credit_points)/SUM(s.credit_points) else '0' end,3,0) *10 as GPAPercentage,(select LTRIM(RTRIM(ISNULL(classification,''))) as classification from coe_classification where ( ROUND(case when ISNULL(SUM(s.credit_points),'0')<>'0' then SUM(ROUND(case when ISNULL(s.maxtotal,'0')<> '0' then (case when isnull(m.total,0) >=0  then isnull(m.total,0) else 0 end * 10)/s.maxtotal else 0 end,1,0)* s.credit_points)/SUM(s.credit_points) else '0' end,3,0) *10) between frompoint and topoint and coe_classification.batch_year='" + batchYear + "' and coe_classification.collegecode='" + collCode + "' and edu_level='" + eduLevel + "') as gpaClassification,(select LTRIM(RTRIM(ISNULL(grade,''))) as grade from coe_classification where (ROUND(case when ISNULL(SUM(s.credit_points),'0')<>'0' then SUM(ROUND(case when ISNULL(s.maxtotal,'0')<> '0' then (case when isnull(m.total,0) >=0  then isnull(m.total,0) else 0 end * 10)/s.maxtotal else 0 end,1,0)* s.credit_points)/SUM(s.credit_points) else '0' end,3,0) *10) between coe_classification.frompoint and coe_classification.topoint and coe_classification.batch_year='" + batchYear + "' and coe_classification.collegecode='" + collCode + "' and edu_level='" + eduLevel + "') as gpaGrade from registration r, mark_entry m,subject s,syllabus_master sm,sub_sem ss,Exam_Details ed,grade_master gm where gm.batch_year=r.Batch_Year and gm.batch_year=ed.batch_year and gm.batch_year=sm.Batch_Year and    gm.degree_code=r.degree_code and gm.degree_code=sm.degree_code and  s.subject_no=m.subject_no and m.exam_code=ed.exam_code and s.syll_code=sm.syll_code and s.syll_code=ss.syll_code and ss.syll_code=s.syll_code and ss.subType_no=s.subType_no and ed.batch_year=sm.Batch_Year and ed.degree_code=sm.degree_code and r.roll_no=m.roll_no and r.Batch_Year=sm.Batch_Year and r.degree_code=sm.degree_code and m.external_mark is not null and m.total is not null and m.result is not null and m.result='pass'  and ed.batch_year='" + batchYear + "' and ed.degree_code='" + degreeCode + "'  /*  and r.reg_no in(@regNo)*/ group by m.roll_no,r.Reg_No,r.App_No,r.Stud_Name,s.Part_Type  order by r.Reg_No,GPAPercentage desc ";
            DataTable dtStudent = dir.selectDataTable(selectQ);
            DataTable dtdicStud = dtStudent.DefaultView.ToTable(true, "roll_no", "Reg_No", "Stud_Name");
            int cun = 0;
            if (dtStudent.Rows.Count > 0)
            {
                foreach (DataRow drStu in dtdicStud.Rows)
                {
                    cun++;
                    string rollNo = Convert.ToString(drStu["Roll_No"]);
                    string rollNoNew = Convert.ToString(drStu["Roll_No"]);
                    string Reg_No = Convert.ToString(drStu["Reg_No"]);
                    string Stud_Name = Convert.ToString(drStu["Stud_Name"]);
                    drNew = dtReport.NewRow();
                    drNew["S.No"] = cun;
                    drNew["Register No"] = Reg_No;
                    drNew["Roll No"] = rollNo;
                    drNew["Student Name"] = Stud_Name;
                    dtReport.Rows.Add(drNew);
                    for (int col = 4; col < dtReport.Columns.Count; col++)
                    {
                        string colName = Convert.ToString(dtReport.Columns[col]);
                        if (colName.Contains('-'))
                        {
                            string[] val = colName.Split('-');
                            string PartNo = Convert.ToString(val[1]);
                            dtStudent.DefaultView.RowFilter = "Roll_No='" + rollNo + "' and part_type='" + PartNo + "'";
                            DataView dvGrade = dtStudent.DefaultView;
                            if (dvGrade.Count > 0)
                            {
                                string gpa = Convert.ToString(dvGrade[0]["GPAResult"]);
                                string grade = Convert.ToString(dvGrade[0]["GPAGrade"]);
                                double CGPA = 0;
                                double.TryParse(gpa, out CGPA);
                                CGPA = Math.Round(CGPA, 1, MidpointRounding.AwayFromZero);
                                string txt = Convert.ToString(dtReport.Rows[1][col]);
                                if (txt.Trim() == "CGPA")
                                    dtReport.Rows[dtReport.Rows.Count - 1][col] = CGPA.ToString();
                                else if (txt.Trim() == "GRADE")
                                    dtReport.Rows[dtReport.Rows.Count - 1][col] = grade;
                                else if (txt.Trim() == "GP")
                                    dtReport.Rows[dtReport.Rows.Count - 1][col] = "";
                            }
                        }
                    }
                 

                }

                if (dtReport.Rows.Count > 0)
                {
                    GridView1.DataSource = dtReport;
                    GridView1.DataBind();
                    GridView1.Visible = true;
                    int rowcnt = GridView1.Rows.Count - 2;
                    int colcnt = GridView1.Columns.Count;
                    //Rowspan
                    for (int rowIndex = GridView1.Rows.Count - rowcnt - 1; rowIndex >= 0; rowIndex--)
                    {
                        GridViewRow row = GridView1.Rows[rowIndex];
                        GridViewRow previousRow = GridView1.Rows[rowIndex + 1];
                        GridView1.Rows[rowIndex].BackColor = ColorTranslator.FromHtml("#0CA6CA");
                        GridView1.Rows[rowIndex].Font.Bold = true;
                        GridView1.Rows[rowIndex].HorizontalAlign = HorizontalAlign.Center;
                        if (rowIndex == 3)
                        {
                            for (int i = 0; i < colcnt; i++)
                            {
                                if (row.Cells[i].Text == previousRow.Cells[i].Text)
                                {

                                    row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 :
                                                           previousRow.Cells[i].RowSpan + 1;
                                    previousRow.Cells[i].Visible = false;
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0; i < row.Cells.Count; i++)
                            {
                                if (row.Cells[i].Text == previousRow.Cells[i].Text)
                                {
                                    row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 :
                                                           previousRow.Cells[i].RowSpan + 1;
                                    previousRow.Cells[i].Visible = false;
                                }

                            }
                        }

                    }

                    //ColumnSpan
                    for (int rowIndex = GridView1.Rows.Count - rowcnt - 2; rowIndex >= 0; rowIndex--)
                    {


                        for (int cell = GridView1.Rows[rowIndex].Cells.Count - 1; cell > 0; cell--)
                        {
                            TableCell colum = GridView1.Rows[rowIndex].Cells[cell];
                            TableCell previouscol = GridView1.Rows[rowIndex].Cells[cell - 1];
                            if (colum.Text.Trim() == previouscol.Text.Trim())
                            {
                                if (previouscol.ColumnSpan == 0)
                                {
                                    if (colum.ColumnSpan == 0)
                                    {
                                        previouscol.ColumnSpan += 2;

                                    }
                                    else
                                    {
                                        previouscol.ColumnSpan += colum.ColumnSpan + 1;

                                    }
                                    colum.Visible = false;

                                }
                            }
                        }

                    }
                }

            }
        }
        catch
        {

        }
    }

    private static void AddTableColumn(DataTable resultsTable, StringBuilder ColumnName)
    {
        try
        {
            DataColumn tableCol = new DataColumn(ColumnName.ToString());
            resultsTable.Columns.Add(tableCol);
        }
        catch (System.Data.DuplicateNameException)
        {
            ColumnName.Append(" ");
            AddTableColumn(resultsTable, ColumnName);
        }
    }
}